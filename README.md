### Overview

You must write an application that filters a log file
by different properties.

A log file contains a header line, followed by zero or more
data lines, in CSV format. The data lines are not guaranteed
to be in any particular order, but you can assume the order
of the columns will always be the same.

An example file is:
```
REQUEST_TIMESTAMP,COUNTRY_CODE,RESPONSE_TIME
1546619278,GB,200
```

The features that you have to implement are defined in
`com.surecloud.DataFilterService`. 

You must implement the features:
* without changing the `DataFilterService` method signatures
* without adding any new dependencies
* with evidence to show that the features work as expected
