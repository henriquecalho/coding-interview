package com.surecloud;

import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.*;

public class DataFilterServiceTest
{
    private DataFilterService dateFilterService = new DataFilterService();

    @Test
    public void emptyFile()
                    throws FileNotFoundException
    {
        assertTrue( dateFilterService.filterByCountry( openFile( "src/test/resources/empty.txt" ), "GB" ).isEmpty() );
    }

    @Test
    public void singleLineFileMatch() throws IOException {
        Collection<String> expected = Files.readAllLines(Paths.get("src/test/resources/single-line.txt"))
                .subList(1, 2);
        Collection<?> actual = dateFilterService.filterByCountry(openFile("src/test/resources/single-line.txt" ), "GB");
        assertEquals(expected, actual);
    }

    @Test
    public void singleLineFileNoMatch() throws IOException {
        Collection<String> expected = Files.readAllLines(Paths.get("src/test/resources/single-line.txt"))
                .subList(1, 2);
        Collection<?> actual = dateFilterService.filterByCountry(openFile("src/test/resources/single-line.txt" ), "PT");
        assertNotEquals(expected, actual);
    }

    @Test
    public void multiLinesFileMatch() throws IOException {
        List<String> expected = Files.readAllLines(Paths.get("src/test/resources/multi-lines.txt"));
        expected.remove(0);
        expected.remove(1);
        expected.remove(3);

        Collection<?> actual = dateFilterService.filterByCountry(openFile("src/test/resources/multi-lines.txt" ), "US");
        assertEquals(expected, actual);
    }

    @Test
    public void multiLinesFileNoMatch() throws IOException {
        List<String> expected = Files.readAllLines(Paths.get("src/test/resources/multi-lines.txt"));
        expected.remove(0);
        expected.remove(1);
        expected.remove(3);

        Collection<?> actual = dateFilterService.filterByCountry(openFile("src/test/resources/multi-lines.txt" ), "DE");
        assertNotEquals(expected, actual);
    }

    private FileReader openFile( String filename )
                    throws FileNotFoundException
    {
        return new FileReader( new File( filename ) );
    }
}

