package com.surecloud;

import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import static org.junit.Assert.*;

public class LogParserTest {

    @Test
    public void emptyFile() throws FileNotFoundException {
        FileReader source = openFile("src/test/resources/empty.txt");
        LogParser logParser = new LogParser(source);
        Map<String, Integer> header = logParser.tryParseHeader();
        boolean hasHeader = header != null;
        assertFalse(hasHeader);
    }

    @Test
    public void headerIsPresent() throws FileNotFoundException {
        FileReader source = openFile("src/test/resources/single-line.txt");
        LogParser logParser = new LogParser(source);
        Map<String, Integer> header = logParser.tryParseHeader();
        boolean hasHeader = header != null;
        logParser.tryCloseResources();
        assertTrue(hasHeader);
    }

    @Test
    public void headerParserIsCorrect() throws FileNotFoundException, NoSuchFieldException, IllegalAccessException {
        FileReader source = openFile("src/test/resources/single-line.txt");
        LogParser logParser = new LogParser(source);
        Map<String, Integer> header = logParser.tryParseHeader();
        boolean hasHeader = header != null;
        assertTrue(hasHeader);

        Map<String, Integer> expected = new HashMap<String, Integer>() {{
            put("REQUEST_TIMESTAMP", 0);
            put("COUNTRY_CODE", 1);
            put("RESPONSE_TIME", 2);
        }};

        Field field = logParser.getClass().getDeclaredField("columnCodes");
        field.setAccessible(true);
        Object actual = field.get(logParser);
        logParser.tryCloseResources();
        assertEquals(expected, actual);
    }

    @Test
    public void onlyHeader() throws FileNotFoundException {
        FileReader source = openFile("src/test/resources/only-header.txt");
        LogParser logParser = new LogParser(source);
        Map<String, Integer> header = logParser.tryParseHeader();
        boolean hasHeader = header != null;
        assertTrue(hasHeader);
        String line = logParser.tryGetNextLine();
        logParser.tryCloseResources();
        assertNull(line);
    }

    @Test
    public void parseWithOneFilters() throws IOException {
        Collection<String> expected = Files.readAllLines(Paths.get("src/test/resources/single-line.txt"))
                .subList(1, 2);

        FileReader source = openFile("src/test/resources/single-line.txt");
        LogParser logParser = new LogParser(source);
        Map<String, Integer> header = logParser.tryParseHeader();
        boolean hasHeader = header != null;
        if(!hasHeader)
            throw new IllegalStateException();

        LogFilterByColumn countryCodeFilter = new LogFilterByColumn(header, "COUNTRY_CODE", "GB");
        List<String> actual = new ArrayList<>();
        String line;

        while((line = logParser.tryGetNextLine(countryCodeFilter)) != null){
            actual.add(line);
        }
        logParser.tryCloseResources();

        assertEquals(expected, actual);
    }

    @Test
    public void parseWithMultipleFilters() throws IOException {
        Collection<String> expected = Files.readAllLines(Paths.get("src/test/resources/single-line.txt"))
                .subList(1, 2);

        FileReader source = openFile("src/test/resources/single-line.txt");
        LogParser logParser = new LogParser(source);
        Map<String, Integer> header = logParser.tryParseHeader();
        boolean hasHeader = header != null;
        if(!hasHeader)
            throw new IllegalStateException();

        LogFilterByColumn countryCodeFilter = new LogFilterByColumn(header, "COUNTRY_CODE", "GB");
        LogFilterByColumn responseTimeFilter = new LogFilterByColumn(header, "RESPONSE_TIME", "200");
        List<String> actual = new ArrayList<>();
        String line;

        while((line = logParser.tryGetNextLine(countryCodeFilter, responseTimeFilter)) != null){
            actual.add(line);
        }
        logParser.tryCloseResources();

        assertEquals(expected, actual);
    }

    private FileReader openFile( String filename )
            throws FileNotFoundException
    {
        return new FileReader( new File( filename ) );
    }
}
