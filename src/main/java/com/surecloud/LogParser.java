package com.surecloud;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.*;
import java.util.stream.Stream;

public class LogParser {

    private final Reader reader;
    private final BufferedReader bufferedReader;
    private final Map<String, Integer> columnCodes;
    public static final String SEPARATOR = ",";

    public LogParser(Reader reader) {
        this.reader = reader;
        this.bufferedReader = new BufferedReader(reader);
        columnCodes = new HashMap<>();
    }

    public Map<String, Integer> parseHeader() throws IOException {
        int[] i = {0};
        String line = bufferedReader.readLine();
        if(line == null)
            throw new IllegalStateException("Header not found.");

        Stream.of(line.split(SEPARATOR))
                .forEach(c -> columnCodes.put(c, i[0]++));
        return columnCodes;
    }

    public String getNextLine(LogFilter... logFilters) throws IOException {
        String line;
        // Keep reading lines until one is find that validates all filters
        while((line = bufferedReader.readLine()) != null) {
            boolean isValid = true;
            for(LogFilter logFilter : logFilters){
                if(!logFilter.validate(line)){
                    isValid = false;
                    break;
                }
            }
            // Found the next line which validates all filters
            if(isValid) return line;
        }
        // Reached end of file
        return null;
    }

    public Map<String, Integer> tryParseHeader(){
        try {
            return parseHeader();
        }
        catch (Exception e){
            // Handle exception
            e.printStackTrace();
            return null;
        }
    }

    public String tryGetNextLine(LogFilterByColumn... logFilters){
        try {
            return getNextLine(logFilters);
        }
        catch (Exception e){
            // Handle exception
            e.printStackTrace();
        }
        return null;
    }

    public void tryCloseResources(){
        try {
            closeResources();
        }
        catch (Exception e){
            // Handle exception
            e.printStackTrace();
        }
    }

    public void closeResources() throws IOException {
        bufferedReader.close();
        reader.close();
    }
}
