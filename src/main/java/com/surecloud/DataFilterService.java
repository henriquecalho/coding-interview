package com.surecloud;

import java.io.Reader;
import java.util.*;

public class DataFilterService
{
    private static final String COUNTRY_CODE = "COUNTRY_CODE";

    public Collection<?> filterByCountry( Reader source, String country ) {
        LogParser logParser = new LogParser(source);
        Map<String, Integer> header = logParser.tryParseHeader();
        if(header == null)
            return Collections.emptyList();

        LogFilterByColumn logFilter = new LogFilterByColumn(header, COUNTRY_CODE, country);
        List<String> lines = new ArrayList<>();
        String line;

        while((line = logParser.tryGetNextLine(logFilter)) != null){
            lines.add(line);
        }
        logParser.tryCloseResources();
        return lines;
    }
}