package com.surecloud;

import java.util.Map;

public class LogFilterByColumn implements LogFilter {

    private final Map<String, Integer> columnCodes;
    private String columnCode;
    private String value;

    public LogFilterByColumn(Map<String, Integer> columnCodes, String columnCode, String value) {
        this.columnCodes = columnCodes;
        this.columnCode = columnCode;
        this.value = value;
    }

    public boolean validate(String line){
        String[] columns = line.split(LogParser.SEPARATOR);
        int index = columnCodes.get(columnCode);
        String valueToValidate = columns[index];
        return this.value.equals(valueToValidate);
    }

    public String getColumnCode() { return columnCode; }

    public LogFilterByColumn setColumnCode(String columnCode) { this.columnCode = columnCode; return this; }

    public String getValue() { return value; }

    public LogFilterByColumn setValue(String value) { this.value = value; return this; }
}
