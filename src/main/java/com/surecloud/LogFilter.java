package com.surecloud;

public interface LogFilter {

    boolean validate(String line);
}